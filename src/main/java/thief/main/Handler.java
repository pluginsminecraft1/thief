package thief.main;

import java.util.HashMap;

import thief.main.database.DataBaseLimit;
import thief.main.menus.BlackStoreInventory;
import thief.main.menus.handler.BuyItemsMenuHandler;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import thief.main.menus.BuyItemsMenu;
import thief.main.menus.SellItemsMenu;
import thief.main.menus.handler.BlackStoreInventoryHandler;
import thief.main.menus.handler.SellItemsMenuHandler;

public class Handler implements Listener{
	
	BlackStoreInventory mainInv = BlackStoreInventory.getInstance();
	BuyItemsMenu buyItemsMenu = BuyItemsMenu.getInstance();
	SellItemsMenu sellItemsMenu = SellItemsMenu.getInstance();
	
	DataBaseLimit dataBaseLimit = DataBaseLimit.getInstance();
	
	BlackStoreInventoryHandler mainInvHandler = BlackStoreInventoryHandler.getInstance();
	BuyItemsMenuHandler buyItemsMenuHandler = BuyItemsMenuHandler.getInstance();
	SellItemsMenuHandler sellItemsMenuHandler = SellItemsMenuHandler.getInstance();
	
	public Thief plugin;
	public static HashMap<Player, Boolean> open = new HashMap<Player, Boolean>();
	public Handler(Thief plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		mainInvHandler.onClick(e, this.plugin);
		buyItemsMenuHandler.onClick(e,this.plugin);
		sellItemsMenuHandler.onClick(e, this.plugin);
	}
	
	
	static boolean open(String name) {return open.get(name);}
			
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) { dataBaseLimit.onJoin(e.getPlayer().getName(), 256); }
	
	@EventHandler
	public void openInv(InventoryOpenEvent e) {
		Player p = (Player) e.getPlayer();
		if(e.getInventory().equals(mainInv.inventories.get(p))) {
			open.put(p, true);
		}
	}
	
	@EventHandler
	public void openInv(InventoryCloseEvent e) {
		Player p = (Player) e.getPlayer();
		if(e.getInventory().equals(mainInv.inventories.get(p))) {
			open.put((Player) e.getPlayer(), false);
		}
		if(e.getInventory().equals(buyItemsMenu.inventories.get(p))) {
			buyItemsMenu.inventories.remove(e.getPlayer());
		}
		if(e.getInventory().equals(SellItemsMenu.inventories.get(p))) {
			SellItemsMenu.inventories.remove(e.getPlayer());
		}
	}

}

