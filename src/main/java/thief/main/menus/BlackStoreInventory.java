package thief.main.menus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import thief.main.database.DataBaseLimit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class BlackStoreInventory {
	public HashMap<Player,Inventory> inventories = new HashMap<Player,Inventory>();
	public Inventory inv = null;
	private static BlackStoreInventory instance = null;
	DataBaseLimit dbLimit = DataBaseLimit.getInstance();

	public static BlackStoreInventory getInstance() {
		if (instance == null) {
			instance = new BlackStoreInventory();
		}

		return instance;

	}
	
	public String getTime(JavaPlugin plugin) {
		int time = plugin.getConfig().getInt("time");
		
		int hours = time / 3600;
		int minutes = (time % 3600) / 60;
		int seconds = time % 60;
		
		String timeFormat = String.format("%02d:%02d:%02d", hours, minutes, seconds);
		return timeFormat;
	}
	public void openMenu(Player player, JavaPlugin plugin) {

    	player.openInventory(createMenu(plugin, player));
    	
	}
	public Inventory createMenu(JavaPlugin plugin, Player player) {
		ConfigurationSection section =  plugin.getConfig().getConfigurationSection("mainMenuFill");
    	String name = plugin.getConfig().getString("menus.main");
    	name = name.replace("&", "\u00a7");
    	inv = Bukkit.createInventory(player, 45, name);
    	
		for(String s: section.getKeys(false)) {
			
			ConfigurationSection items =  plugin.getConfig().getConfigurationSection("mainMenuFill.items");
			for(String i: items.getKeys(false)) {
						
					ItemStack item = new ItemStack(Material.valueOf(plugin.getConfig().getString("mainMenuFill.items."+i+".material")));
					ItemMeta itemMeta = item.getItemMeta();
					itemMeta.setDisplayName(plugin.getConfig().getString("mainMenuFill.items."+i+".displayName").replace("&", "\u00a7"));

					if(plugin.getConfig().getString("mainMenuFill.items."+i+".lore") != null && plugin.getConfig().getString("mainMenuFill.items."+i+".lore").equals("false")) {

					}else {
						List<String> lore = (List<String>) plugin.getConfig().getList("mainMenuFill.items."+i+".lore");	

						lore = lore.stream().map(p -> p.replace("&", "\u00a7").replace("%limit%", String.valueOf(dbLimit.getLimit(player.getName()))
								.replace("%time%", getTime(plugin)))).collect(Collectors.toList());
						itemMeta.setLore(lore);
					}
					item.setItemMeta(itemMeta);
					
					ArrayList<Integer> slots = (ArrayList<Integer>) plugin.getConfig().getIntegerList("mainMenuFill.items."+i+".slots");
				
					for(int slot: slots) {
						inv.setItem(slot, item);
					}
				}
			
			
		}
	    inventories.put(player, inv);
		return inv;
		
	}
	
}
