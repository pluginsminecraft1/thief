package thief.main.menus.handler;

import org.bukkit.inventory.ItemStack;
import thief.main.database.DataBaseBuy;
import thief.main.database.DataBaseLimit;
import thief.main.database.DataBaseSell;
import thief.main.Thief;
import thief.main.menus.BlackStoreInventory;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.plugin.java.JavaPlugin;

import thief.main.menus.BuyItemsMenu;
import thief.main.menus.SellItemsMenu;
import net.milkbowl.vault.economy.Economy;
import thief.main.obj.Items;

import java.util.List;

public class SellItemsMenuHandler {

	private final Economy econ = Thief.getEconomy();

	BlackStoreInventory mainInv = BlackStoreInventory.getInstance();
	DataBaseBuy dbBuy = DataBaseBuy.getInstance();
	DataBaseSell dbSell = DataBaseSell.getInstance();
	BuyItemsMenu buyItems = BuyItemsMenu.getInstance();
	SellItemsMenu sellItems = SellItemsMenu.getInstance();
	DataBaseLimit dbLimit = DataBaseLimit.getInstance();

	private static SellItemsMenuHandler instance = null;

	public static SellItemsMenuHandler getInstance() {
		if (instance == null) {
			instance = new SellItemsMenuHandler();
		}

		return instance;

	}

	public void onClick(InventoryClickEvent e, JavaPlugin plugin) {
		Player p = (Player) e.getWhoClicked();
		if (e.getInventory().equals(sellItems.inventories.get(p))) {
			if (e.getSlot() == 18) {
				if (mainInv.inventories.isEmpty() || mainInv.inventories.get(p) == null) {
					mainInv.createMenu(plugin, p);
				}

				p.openInventory(mainInv.inventories.get(p));
			}
			if (e.getSlot() == 11 || e.getSlot() == 15 || e.getSlot() == 22 || e.getSlot() == 29 || e.getSlot() == 33) {
				List<Items> items = dbSell.getSavedSellItems();
				for (Items savedSellItem : items) {

					ItemStack item = new ItemStack(savedSellItem.getItem().getType());
					int price = savedSellItem.getPrice();
					if (e.getCurrentItem().getType().equals(item.getType())) {
						if (dbLimit.getLimit(p.getName()) >= e.getCurrentItem().getAmount()) {
							int amount = e.getCurrentItem().getAmount();

							String priceFull = String.valueOf(price * amount);
							if(p.getInventory().containsAtLeast(item, amount)) {
								item.setAmount(amount);
								p.getInventory().removeItem(item);
								econ.depositPlayer(p, price * amount);
								String successSell = plugin.getConfig().getString("successSell");
								successSell = successSell.replace("&", "\u00a7").replace("%price%", priceFull);
								p.sendMessage(successSell);
								dbLimit.setLimit(p.getName(), dbLimit.getLimit(p.getName()) - e.getCurrentItem().getAmount());
							} else {
								String invalidSell = plugin.getConfig().getString("invalidSell");
								invalidSell = invalidSell.replace("&", "\u00a7");
								p.sendMessage(invalidSell);
							}
							e.setCancelled(true);
							return;

						} else {
							String messageLimit = plugin.getConfig().getString("limitExceeded");
							messageLimit = messageLimit.replace("&", "\u00a7").replace("%limit%",
									String.valueOf(dbLimit.getLimit(p.getName())));
							p.sendMessage(messageLimit);
						}
					}
				}
			}
			if (e.getSlot() == 17) {
				increaseItem.increase(dbSell.getSavedSellItems(), e, plugin, "sellItemsLore");
			}
			if (e.getSlot() == 35) {
				decreaseItem.decrease(dbSell.getSavedSellItems(), e, plugin, "sellItemsLore");
			}
			e.setCancelled(true);
		}
}
}
