package thief.main.menus.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.entity.Item;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import thief.main.obj.Items;

public class decreaseItem {
	public static void decrease(List<Items> map, InventoryClickEvent e, JavaPlugin plugin,
								String loreString) {
		int[] slots = { 11, 15, 22, 29, 33 };

		for (int i = 0; i < slots.length; i++) {

			if (e.getClickedInventory().getItem(slots[i]).getAmount() != 1) {

				ItemStack currentItem = e.getClickedInventory().getItem(slots[i]);
				int amount = currentItem.getAmount() - 1;
				ItemMeta itemMeta = currentItem.getItemMeta();

				for (Items items : map) {
					ItemStack item = items.getItem();
					int price = items.getPrice();

				if (currentItem.getType().equals(item.getType())) {
					currentItem.setAmount(amount);
					String priceFull = String.valueOf(price * amount);

					ArrayList<String> lore = (ArrayList<String>) plugin.getConfig().getList(loreString);
					lore = (ArrayList<String>) lore.stream()
							.map(q -> q.replace("&", "\u00a7").replace("%price%", priceFull))
							.collect(Collectors.toList());
					itemMeta.setLore(lore);

					currentItem.setItemMeta(itemMeta);

				}

			}
		}
	}

}
}
