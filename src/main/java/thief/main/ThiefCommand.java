package thief.main;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import thief.main.database.DataBaseBuy;
import thief.main.database.DataBaseSell;
import thief.main.menus.BlackStoreInventory;
import thief.main.obj.SaveConfig;

public class ThiefCommand implements CommandExecutor {
	BlackStoreInventory mainInv = BlackStoreInventory.getInstance();

	DataBaseSell dbSell = DataBaseSell.getInstance();
	DataBaseBuy dbBuy = DataBaseBuy.getInstance();

	public Thief plugin;

	public ThiefCommand(Thief plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {

		if (sender instanceof Player) {

			if (args.length == 1) {
				if (sender.isOp() && args[0].equals("reload")) {
					sender.sendMessage("Перезагрузка плагина");

					this.plugin.reloadConfig();
					this.plugin.saveConfig();

					return false;

				}
				if(sender.isOp() && args.length == 1 && args[0].equals("create")) {
					dbSell.createSellItems();
					dbBuy.createBuyItems();
				}
				
			}
			Player p = (Player) sender;
			if(mainInv.inventories.isEmpty() || mainInv.inventories.get(p) == null) {
				mainInv.createMenu(this.plugin, p);
			}

			p.openInventory(mainInv.inventories.get(p));
		}

		else if (args.length == 1) {
			if (args[0].equals("reload")) {
				sender.sendMessage("Перезагрузка плагина");

				this.plugin.reloadConfig();
				this.plugin.saveConfig();

			}
		}

		return false;
	}

}
