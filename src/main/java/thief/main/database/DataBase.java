package thief.main.database;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DataBase {
    private File dbFile = null;
    private Connection connection;
    private static DataBase instance = null;
    private String dir = System.getProperty("user.dir");;

    public static DataBase getInstance() {
        if (instance == null) {
            instance = new DataBase();
        }

        return instance;

    }
    public Connection getConnection(){
        if(connection == null){
            try{
                Class.forName( "org.sqlite.JDBC");
                connection = DriverManager.getConnection("JDBC:sqlite:plugins/Thief/database.db", null,
                        null);
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        return connection;
    }
    public void createDB() {
        if (dbFile == null) {
            File db = new File(dir + File.separator + "/plugins/Thief/database.db");
            db.mkdirs();
            try {
                db.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            DataBase.getInstance().dbFile = db;
        }

    }

    public void createTables() {
        try {

            Statement statement = getConnection().createStatement();

            statement.executeUpdate("CREATE TABLE IF NOT EXISTS thief_limit(player varchar(255), `limit` int)");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS buy_items(name varchar(255), `min_price` int,`max_price` int)");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS sell_items(name varchar(255), `min_price` int,`max_price` int)");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS saved_buyitems(`id` int, name varchar(255),`price` int)");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS saved_sellitems(`id` int, name varchar(255),`price` int)");

        } catch (SQLException e) {

            e.printStackTrace();
        }


    }

}
