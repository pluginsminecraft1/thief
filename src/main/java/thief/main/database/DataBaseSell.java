package thief.main.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import thief.main.obj.Items;

import javax.xml.crypto.Data;

public class DataBaseSell {

	public Connection connection;
	DataBase db = DataBase.getInstance();
	private static DataBaseSell instance = null;
	public List<Items> items = new ArrayList<>();
	public final int[] slotsNumber = { 11, 15, 22, 29, 33 };

	public static DataBaseSell getInstance() {
		if (instance == null) {
			instance = new DataBaseSell();
		}

		return instance;

	}

	public int getRandom(int min, int max) {

		return (int) ((Math.random() * (max - min)) + min);

	}

	public HashMap<ItemStack, Integer> getItemsSell() {
		HashMap<ItemStack, Integer> itemsMap = new HashMap<ItemStack, Integer>();
		ArrayList<Integer> ids = new ArrayList<Integer>();

		try {

			for (int i = 0; i < 5; i++) {
				Statement statement = db.getConnection().createStatement();
				int rand = getRandom(1, 11);
				while (ids.contains(rand)) {
					rand = getRandom(1, 11);
				}
				ResultSet res = statement
						.executeQuery("SELECT `name`, `min_price`, `max_price` FROM `sell_items` WHERE `id` = " + rand);

				res.next();

				String material = res.getString("name").toUpperCase();
				int min_price = res.getInt("min_price");
				int max_price = res.getInt("max_price");

				int randomPrice = getRandom(min_price, max_price);

				ItemStack item = new ItemStack(Material.valueOf(material));
				itemsMap.put(item, randomPrice);
				ids.add(rand);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return itemsMap;

	}

	int j = 1;

	public int id(int i) {
		int m = i + 1;
		return m;
	}

	public void createSellItems() {

		HashMap<ItemStack, Integer> itemsMap = getItemsSell();

		try {

			Statement statement = db.getConnection().createStatement();

			statement.executeUpdate("DELETE FROM `saved_sellitems`");
			itemsMap.forEach((item, price) -> {

				String material = String.valueOf(item.getType());
				try {
					statement.executeUpdate("INSERT INTO `saved_sellitems` (`id`, `name`, `price`) VALUES (" + j + ",'"
							+ material + "', " + price + ")");
					j = id(j);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			});

		} catch (SQLException e) {

			e.printStackTrace();
		}
		j = 1;
		createSavedSellItems();
	}
	public List<Items> getSavedSellItems(){
		return items;
	}
	public void createSavedSellItems() {

		items.clear();
		try {
			Statement statement = db.getConnection().createStatement();
			int j = 0;
			for (int i = 1; i < 6; i++) {

				ResultSet res = statement
						.executeQuery("SELECT `name`, `price` FROM `saved_sellitems` WHERE `id` = " + i);

				res.next();

					ItemStack mat = new ItemStack(Material.valueOf(res.getString("name")));
					int price = res.getInt("price");
					int slot = slotsNumber[j];
					j++;
					Items item = new Items(price, mat, slot);
					items.add(item);


			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
