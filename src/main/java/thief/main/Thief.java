package thief.main;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import net.milkbowl.vault.economy.Economy;
import thief.main.database.DataBase;
import thief.main.database.DataBaseBuy;
import thief.main.database.DataBaseLimit;
import thief.main.database.DataBaseSell;

public class Thief extends JavaPlugin {
	private static Economy economy = null;
	DataBase db = DataBase.getInstance();
	DataBaseBuy dbBuy = DataBaseBuy.getInstance();
	DataBaseSell dbSell = DataBaseSell.getInstance();

	public DataBaseLimit dataBaseLimit = DataBaseLimit.getInstance();
	public void onEnable() {

		File config = new File(getDataFolder() + File.separator + "config.yml");
		if (!config.exists()) {
			getConfig().options().copyDefaults(true);
			saveDefaultConfig();
		}

		getCommand("thief").setExecutor(new ThiefCommand(this));
		
		if (!setupEconomy()) {
			System.out.println("Vault not found!");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
		db.createDB();
		db.createTables();
		dbBuy.createSavedBuyItems();
		dbSell.createSavedSellItems();

		Bukkit.getPluginManager().registerEvents(new Handler(this), this);
		Timer.startTimer(this);
		System.out.print("Timer Enable");
	}

	private boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager()
				.getRegistration(Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}

		return (economy != null);
	}

	public static Economy getEconomy() {
		return economy;
	}

	public void onDisable(){
		this.getConfig().set("time", Timer.time);
		this.saveConfig();
	}

}
